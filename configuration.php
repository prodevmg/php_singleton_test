<?php

class Config implements ArrayAccess
{
    protected static $instance = null;
    protected $_data= array();
    
    /**
     * Call this method to get singleton
     *
     * @return @var
     */
    public static function singleton()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    protected function __construct()
    {
        // No constructor!
    }

    protected function __clone()
    {
        //No clones!
    }
    
    /**
     * Array Accessor Functions
     * 
     **/
     
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_data[] = $value;
        } else {
            $this->_data[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->_data[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->_data[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
    }
    
    /**
     * End Array Accessor
     * 
     **/
     
     
     /**
     * Mutator/ Magic Methods
     * 
     **/
 
	/**
     * Call this method to set a single property via object accessor
     *
     * @return bool
     */
	public function __set($property, $value) {
			
			// No direct setting of data
		if($property === '_data') {
			return false;
		} else {
			return $this->set($property, $value);
			}
	 
	}
	
	/**
     * Call this method to unset a single property
     *
     * @return bool
     */
	public function __unset($property) {
			
			// No direct setting of data
		if($property === '_data') {
			return false;
		} else {
			unset($this->_data[$property]);
			return true;
			}
	 
	}
	 
	 /**
     * Call this method to set a get a property via object accessors
     *
     * @return bool
     */
	public function __get($property) {
		
		if($property === '_data') {
			return false;
		} else {
			return $this->get($property);
			}
				
		
	}
	
	/**
     * This method encodes the config list as json
     *
     * @return string
     */
	public function __toString()
    {
        return json_encode($this->_data);
    }
    
    /**
     * Dynamic Function to check whether key exists
     * 
     * @return bool
     **/
     
     public function __call($name, $arguments) {
     	
        $endsWithPostfix = false;
        $postfix = 'Exists';
        $length = strlen($postfix);
        
        $prefixLength = strlen($name) - $length;
	    $prefix = substr($name,0,$prefixLength);
        
        
        
		if ($length == 0) {
			return true;
		}
		
		$endsWithPostfix = (substr($name, -$length) === $postfix);
        
        if($endsWithPostfix) // check if name ends with Exists
        {
	        
	        return array_key_exists($prefix,$this->_data);
        }
        else
        	throw new Exception('Function does not exist.');
        
    }
	
	/**
     * End Mutator/ Magic Methods
     * 
     **/

    /**
     * This method inserts a key value pair into the _data variable
     * 
     * @return bool
     **/
    public function set($k,$v)
    {
    	$keys = array($k);
		return $this->_data = array_fill_keys($keys, $v);
    }
    
    /**
     * This method inserts muliple key value pairs into the _data variable
     * 
     * @return bool
     **/
    public function setValues($arg)
    {
    	// Checks for Array Notation
    	if(is_array($arg)){
    		return $this->_data = array_merge($this->_data,$arg);
    	}
    	
    	// Checks for Object Notation
    	else if(is_object($arg)){
    		return $this->_data = array_merge($this->_data,(array)$arg);
    	}
    	
    	else {
    		throw new Exception('Not an object or array.');
    	}
    	
    	

    }
    
    /**
     * This method retrieves a key value pair from the _data variable based on key
     * 
     * @return bool
     **/
    public function get($key, $default = false)
    {
    	$numargs = func_num_args(); // checking the number of arguments
    	
    	if(isset($this->_data[$key]))
    		return $this->_data[$key];
    		
    	else if(!isset($this->_data[$key]) && $numargs === 2) // if the value is not set but default is given
    		return $default;
    		
    	else { 
    		throw new Exception('Default nor Value is set.');
    	}
    }
    
    
    /**
     * This method loads Json and sets the keys and values into the _data variable
     * 
     * @return bool
     **/
    public function loadJson($arg){
    	$json = json_decode($arg, true);
    	return $this->setValues($json);
    }
    
    

}
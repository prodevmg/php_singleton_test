<?php

require_once('configuration.php');

assert_options(ASSERT_ACTIVE, 1) and assert_options(ASSERT_BAIL, 1);

$config = Config::singleton();
assert($config === Config::singleton());

$config->set('foo', true);
assert($config['foo'] === true);

$config['path'] = '/var/monkey/banana';
assert($config->get('path') == '/var/monkey/banana');

$config->foo = false;
assert($config['foo'] === false);

foreach ( $config as $key => $value ) {
assert(isset($config->$key));
assert($config->get($key) === $value);
}

$config->setValues(array('animal1' =>'monkey', 'animal2' => 'donkey'));
assert($config->animal1 === 'monkey');
assert($config['animal2'] === 'donkey');

try {
$config->setValues('not an array');
assert(false); // setValues did not throw an exception
}
catch ( Exception $e ) {
// passed
}
unset($config->path);

$json = '{"foo":false,"animal1":"monkey","animal2":"donkey"}';
$config->loadJson($json); //added
assert((string) $config === $json);

$config->loadJson('{"foo":true,"animal3":"gorilla"}');
assert($config->animal1 === 'monkey');
assert($config['animal3'] === 'gorilla');
assert($config->get('foo') === true);

assert($config->get('not set', 'myDefault') === 'myDefault');
assert($config->get('not set', null) === null);
assert($config->get('not set', false) === false);

try {
$config->get('not set');
assert(false); // no default and no key set did not throw an exception
}
catch ( Exception $e ) {
// passed
}
try {
$config->notSet;
assert(false); // no default and no key set did not throw an exception
}
catch ( Exception $e ) {
// passed
}

assert($config->fooExists() === true);
assert($config->fakeKeyExists() === false);

try {
$config->fakeMethod();
assert(false); // Non-existant method did not throw an exception
}
catch ( Exception $e ) {
// passed
}

print "All Tests Passed\n";
A basic configuration class that allows setting and getting of key value pairs

Implements the Singleton pattern to ensure that only one instance of the class is instantiated

Implements the Iterator interface to iterate over the keys and values in the config

Allow for getting and setting values with array notation AND object notation

get() should accept a default as the second parameter and used if a value does not exist

get() should throw an exception if there is no value and no default

Allows for checking if a key exists by using a function <key name>Exists()

Load JSON serialized data and export JSON encoded when cast to a string

Uses the PEAR coding standards

Must work with PHP 5.2+ and not use any extensions or external dependencies


